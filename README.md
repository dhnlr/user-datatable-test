# User Datatable
User Datatable is project to show user data from randomuser.me API as datatable

## Features
- Search keywords
- Filter gender
- Pagination
- Column sort

## Example
You can access demo on [GitLab](https://dhnlr.gitlab.io/user-datatable-test/) or [Netlify](https://keen-nobel-e291d2.netlify.app/)

## Setup
### Developement
Clone repo and run ```npm install``` to install package.
Run ```npm start``` and open ```http://localhost:3000```

### Deployment
Build project using ```npm run build```
You can use service like Vercel, Netlify or other service or using preferred web server on your server

## Build With
- React
- React-redux
- randomuser.me
