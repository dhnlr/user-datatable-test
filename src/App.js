import { useDispatch, useSelector } from "react-redux";

import {
  selectKeywords,
  resetFilter,
  selectPage,
  selectGender,
  setPage,
  setKeywords,
  setGender,
  selectSortBy,
  selectSortOrder,
  setSortBy,
  setSortOrder,
} from "./reducer/userSlice";
import useFetchUser from "./hooks/useFetchUser";
import Input from "./components/Input";
import Select from "./components/Select";

function App() {
  const dispatch = useDispatch();
  let keywords = useSelector(selectKeywords);
  let page = useSelector(selectPage);
  let gender = useSelector(selectGender);
  let sortBy = useSelector(selectSortBy);
  let sortOrder = useSelector(selectSortOrder);
  const { isLoading, isError, users } = useFetchUser(
    keywords,
    gender,
    page,
    sortBy,
    sortOrder
  );

  const handleKeywordsChange = (e) => {
    dispatch(setKeywords(e.target.value));
  };

  const handleGenderChange = (e) => {
    dispatch(setGender(e.target.value));
  };

  const handleResetFilter = () => {
    dispatch(resetFilter());
  };

  const handlePageChange = (sign) => {
    if (sign === "+") {
      dispatch(setPage(page + 1));
    } else {
      dispatch(setPage(page - 1));
    }
  };

  const handleSortChange = (by, order) => {
    dispatch(setSortBy(by));
    dispatch(setSortOrder(order));
  };

  return (
    <div className="App">
      <p className="title-desc">Table</p>
      <h1>Users</h1>
      {users.length > 0 && (
        <div>
          <div className="filters">
            <Input
              class={"filter-item"}
              id={"keywords"}
              label={"Search"}
              placeholder={"Username/name/email"}
              handleChange={handleKeywordsChange}
              value={keywords}
            />
            <Select
              class={"filter-item"}
              id={"gender"}
              label={"Gender"}
              handleChange={handleGenderChange}
              value={gender}
              options={[
                { value: "", name: "All" },
                { value: "female", name: "Female" },
                { value: "male", name: "Male" },
              ]}
            />
            <div className="filter-item">
              <button onClick={handleResetFilter}>Reset Filter</button>
            </div>
          </div>
          <div className="table-container">
            <table>
              <thead>
                <tr>
                  <th>
                    <div className="table-head">
                      Username{" "}
                      <div className="order">
                        <button
                          className={
                            sortBy === "username" && sortOrder === "asc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("username", "asc");
                          }}
                        >
                          asc
                        </button>{" "}
                        <button
                          className={
                            sortBy === "username" && sortOrder === "dsc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("username", "dsc");
                          }}
                        >
                          dsc
                        </button>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="table-head">
                      Full Name{" "}
                      <div className="order">
                        <button
                          className={
                            sortBy === "fullname" && sortOrder === "asc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("fullname", "asc");
                          }}
                        >
                          asc
                        </button>{" "}
                        <button
                          className={
                            sortBy === "fullname" && sortOrder === "dsc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("fullname", "dsc");
                          }}
                        >
                          dsc
                        </button>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="table-head">
                      Email{" "}
                      <div className="order">
                        <button
                          className={
                            sortBy === "email" && sortOrder === "asc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("email", "asc");
                          }}
                        >
                          asc
                        </button>{" "}
                        <button
                          className={
                            sortBy === "email" && sortOrder === "dsc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("email", "dsc");
                          }}
                        >
                          dsc
                        </button>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="table-head">
                      Gender{" "}
                      <div className="order">
                        <button
                          className={
                            sortBy === "gender" && sortOrder === "asc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("gender", "asc");
                          }}
                        >
                          asc
                        </button>{" "}
                        <button
                          className={
                            sortBy === "gender" && sortOrder === "dsc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("gender", "dsc");
                          }}
                        >
                          dsc
                        </button>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="table-head">
                      Registered Date{" "}
                      <div className="order">
                        <button
                          className={
                            sortBy === "registration_date" &&
                            sortOrder === "asc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("registration_date", "asc");
                          }}
                        >
                          asc
                        </button>{" "}
                        <button
                          className={
                            sortBy === "registration_date" &&
                            sortOrder === "dsc"
                              ? "active"
                              : ""
                          }
                          onClick={() => {
                            handleSortChange("registration_date", "dsc");
                          }}
                        >
                          dsc
                        </button>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {users.map((user, index) => (
                  <tr key={index}>
                    <td>{user.login.username}</td>
                    <td>
                      {user.name.first} {user.name.last}
                    </td>
                    <td>{user.email}</td>
                    <td>{user.gender}</td>
                    <td>
                      {new Date(user.registered.date).toLocaleString("id-ID", {
                        timeStyle: "short",
                        dateStyle: "short",
                      })}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <div className="pagination">
            <button
              disabled={page === 1}
              onClick={() => {
                if (page > 1) {
                  handlePageChange("-");
                }
              }}
            >
              -
            </button>
            <p>{page}</p>
            <button onClick={() => handlePageChange("+")}>+</button>
          </div>
        </div>
      )}
      {isLoading && <p>Loading...</p>}
      {isError && <p>Error!</p>}
    </div>
  );
}

export default App;
