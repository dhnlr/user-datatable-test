import { useState, useEffect, useCallback } from "react";
import axios from "axios";

const useFetchUser = (keyword, gender, page, sortBy, sortOrder) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [users, setUsers] = useState([]);

  const sendQuery = useCallback(async () => {
    try {
      setIsLoading(true);
      setIsError(false);
      const { data } = await axios.get(
        `https://randomuser.me/api/?page=${page}&results=10&seed=ajaib&gender=${gender}&keyword=${keyword}&sortBy=${sortBy}&sortOrder=${sortOrder}`
      );
      setUsers(data.results);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(error);
    }
  }, [gender, page, keyword, sortBy, sortOrder]);

  useEffect(() => {
    sendQuery();
  }, [gender, page, keyword, sortBy, sortOrder, sendQuery]);

  return { isLoading, isError, users };
};

export default useFetchUser;
