import userReducer, {
  setKeywords,
  setGender,
  setPage,
  resetFilter,
} from "../reducer/userSlice";

describe("user reducer", () => {
  const initialState = {
    users: [],
    keywords: "",
    gender: "",
    page: 1,
    sortBy: "",
    sortOrder: "",
    status: "idle",
  };

  it("should handle initial state", () => {
    expect(userReducer(undefined, { type: "unknown" })).toEqual(initialState);
  });

  it("should change keywords", () => {
    const actual = userReducer(initialState, setKeywords("karen"));
    expect(actual.keywords).toEqual("karen");
  });

  it("should change gender", () => {
    const actual = userReducer(initialState, setGender("female"));
    expect(actual.gender).toEqual("female");
  });

  it("should change page", () => {
    const actual = userReducer(initialState, setPage(3));
    expect(actual.page).toEqual(3);
  });

  it("should reset filtes", () => {
    const changedState = {
      users: [],
      keywords: "karen",
      gender: "female",
      page: 3,
      sortBy: "",
      sortOrder: "",
      status: "idle",
    };
    const actual = userReducer(changedState, resetFilter());
    expect(actual.keywords).toEqual("");
    expect(actual.gender).toEqual("");
  });
});
``;
