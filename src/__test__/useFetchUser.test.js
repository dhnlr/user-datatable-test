import { renderHook } from '@testing-library/react-hooks'
import useFetchUser from "../hooks/useFetchUser";

test('custom hooks hanlde props changes', async () => {
    const { waitForNextUpdate, result } = renderHook(() => useFetchUser(), {initialProps: {
      keyword: "",
      gender: "",
      page: 1,
      sortBy: "",
      sortOrder: ""
    }})
  
    expect(result.current.isLoading).toBe(true)
    expect(result.current.isError).toBe(false)
    expect(result.current.users.length).toBe(0)
  
    await waitForNextUpdate({timeout: 60000})
    expect(result.current.isLoading).toBe(false)
    expect(result.current.isError).toBe(false)
    expect(result.current.users.length).toBe(10)
  })