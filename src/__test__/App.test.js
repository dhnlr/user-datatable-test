import { render as rtlRender, screen } from "@testing-library/react";
import { renderHook } from "@testing-library/react-hooks";
import userEvent from "@testing-library/user-event";
import { Provider } from "react-redux";

import useFetchUser from "../hooks/useFetchUser";
import { store } from "../store";
import App from "../App";

const render = (ui, { ...renderOptions } = {}) => {
  const Wrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );

  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
};

test("renders loading text", () => {
  render(<App />);
  expect(screen.getByText(/loading/i)).toBeInTheDocument();
});

test("render after hook callback", async () => {
  render(<App />);

  const { waitForNextUpdate } = renderHook(() => useFetchUser());

  await waitForNextUpdate({ timeout: 120000 });
  const keywordInput = screen.getByLabelText(/search/i);
  const genderSelect = screen.getByLabelText(/gender/i);
  expect(screen.getByRole("table")).toBeInTheDocument();
  expect(keywordInput).toBeInTheDocument();
  expect(genderSelect).toBeInTheDocument();

  userEvent.type(keywordInput, "karen");
  expect(keywordInput.value).toBe("karen");

  userEvent.selectOptions(genderSelect, 'female')
  expect(genderSelect.value).toBe('female')

  userEvent.click(screen.getByText("+"))
  expect(screen.getByText("2")).toBeInTheDocument()

  userEvent.click(screen.getByText("-"))
  expect(screen.getByText("1")).toBeInTheDocument()
});
