function Input(props) {
  return (
    <div className={props.class}>
      <label htmlFor={props.id}>{props.label}</label>
      <input
        id={props.id}
        placeholder={props.placeholder}
        onChange={props.handleChange}
        value={props.value}
      />
    </div>
  );
}

export default Input;
