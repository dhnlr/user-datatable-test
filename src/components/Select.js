function Select(props) {
  return (
    <div className={props.class}>
      <label htmlFor={props.id}>{props.label}</label>
      <select id={props.id} onChange={props.handleChange} value={props.value}>
        {props.options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.name}
          </option>
        ))}
      </select>
    </div>
  );
}

export default Select;
