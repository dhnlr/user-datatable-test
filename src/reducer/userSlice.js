import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchUser } from "./userAPI";

const initialState = {
  users: [],
  keywords: "",
  gender: "",
  page: 1,
  sortBy: "",
  sortOrder: "",
  status: "idle",
};

export const getUserAsync = createAsyncThunk(
  "movie/fetchUser",
  async (params) => {
    const res = await fetchUser(params.page, params.gender, params.keywords);
    let data = res.data.results ? res.data.results : [];
    return data;
  }
);

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setKeywords: (state, action) => {
      state.keywords = action.payload;
    },
    setGender: (state, action) => {
      state.gender = action.payload;
    },
    setPage: (state, action) => {
      state.page = action.payload;
    },
    setSortBy: (state, action) => {
        state.sortBy = action.payload
    },
    setSortOrder: (state, action) => {
        state.sortOrder = action.payload
    },
    resetFilter: (state) => {
      state.keywords = "";
      state.gender = "";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getUserAsync.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getUserAsync.fulfilled, (state, action) => {
        state.status = "idle";
        state.users = action.payload;
      });
  },
});

export const { setKeywords, setGender, setPage, setSortBy, setSortOrder, resetFilter } =
  userSlice.actions;

export const selectKeywords = (state) => state.user.keywords;
export const selectGender = (state) => state.user.gender;
export const selectPage = (state) => state.user.page;
export const selectUsers = (state) => state.user.users;
export const selectSortBy = (state) => state.user.sortBy
export const selectSortOrder = (state) => state.user.sortOrder

export default userSlice.reducer;
